(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 600,
        HEIGHT = 600;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  var random = (
    d3.random
      .normal(0.5, 0.11)
  );
  var data = (
    d3.range(800)
      .map(function (i) {
        return {
          x: random(),
          y: random()
        }
      })
  );
  var x = (
    d3.scale
      .linear()
      .range([50, WIDTH - 50])
  );
  var y = (
    d3.scale
      .linear()
      .range([HEIGHT - 50, 50])
  );

  svg
    .append("g")
    .classed("circles", true)
    .selectAll("circle")
    .data(data)
    .enter()
    .append("circle")
    .attr({
      cx: function (datum) {
        return x(datum.x)
      },
      cy: function (datum) {
        return y(datum.y)
      },
      r: 4
    });

  svg
    .append("g")
    .classed("axis", true)
    .attr({
      transform: "translate(50, 0)"
    })
    .call(
      d3.svg
        .axis()
        .orient("left")
        .scale(y)
    )

  svg
    .append("g")
    .classed("axis", true)
    .attr({
      transform: `translate(0, ${HEIGHT - 50})`
    })
    .call(
      d3.svg
        .axis()
        .orient("bottom")
        .scale(x)
    );

  svg
    .append("g")
    .classed("brush", true)
    .call(
      d3.svg
        .brush()
        .x(x)
        .y(y)
        .on("brushstart", brushstart)
        .on("brush", brushmove)
        .on("brushend", brushend)
    );

  function brushstart() {
    svg
      .select(".circles")
      .classed("selecting", true);
  }

  function brushmove() {
    var event = (
      d3.event
        .target
        .extent()
    );

    svg
      .selectAll("circle")
      .classed("selected", function (datum) {
        return (
          event[0][0] <= datum.x &&
          datum.x <= event[1][0] &&
          event[0][1] <= datum.y &&
          datum.y <= event[1][1]
        );
      });
  };

  function brushend() {
    svg
      .select(".circles")
      .classed(
        "selecting",
        !d3.event.target.empty()
      )
  };
})();