(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 768;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  var radiate = function (position) {
    d3.range(3)
      .forEach(function (datum) {
        svg
          .append("circle")
          .attr({
            cx: position[0],
            cy: position[1],
            r: 0
          })
          .style({
            opacity: "1"
          })
          .transition()
          .duration(1000)
          .delay(datum * 50)
          .attr({
            r: 50
          })
          .style("opacity", "0.000001")
          .remove();
      });
  };

  svg
    .on("click", function () {
      radiate(d3.mouse(this));
    })

  svg
    .on("touschstart", function () {
      d3.touches(this).map(radiate);
    })
})();