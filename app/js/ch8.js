(function () {
  var xhr = d3.xhr("#"),
      xhrEvents = {
        load: function (request) {
            console.log("Load complete");
            console.log(request);
        },
        error: function (error) {
            console.log(error);
        },
        progress: function (progress) {
            // console.log("Progress");
            console.log(progress.readyState);
        }
      };

  xhr.mimeType("application/json");
  xhr.header("User-Agent", "our example");

  for (event in xhrEvents) {
    if (xhrEvents.hasOwnProperty(event)) {
      xhr.on(event, xhrEvents[event]);
    }
  }

  xhr.send("GET");
})();