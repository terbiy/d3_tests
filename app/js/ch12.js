(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 768;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  var eases = [
    "linear",
    "poly(4)",
    "quad",
    "cubic",
    "sin",
    "circle",
    "elastic(10, -5)",
    "back(0.5)",
    "bounce",
    "cubic-in",
    "cubic-out",
    "cubic-in-out",
    "cubit-out-in"
  ];

  var y = (
    d3.scale
      .ordinal()
      .domain(eases)
      .rangeBands([50, 500])
  );
  var colors = (
    d3.scale
      .category20()
  );
  eases.forEach(function (ease, easeIndex) {
    var transition = (
      svg
        .append("circle")
        .attr({
          cx: 130,
          cy: y(ease),
          r: y.rangeBand() / 2 - 5,
          fill: function () {
            return colors(easeIndex);
          }
        })
        .transition()
        .delay(400)
        .duration(1500)
        .attr({
          cx: 400
        })
    );

    if(ease.indexOf("(") > -1) {
      var args = ease.match(/[0-9]+/g),
          type = ease.match(/^[a-z]+/);

      transition.ease(type, args[0], args[1])
    } else {
      transition.ease(ease);
    }

    svg
      .append("text")
      .text(ease)
      .attr({
        x: 10,
        y: y(ease) + 5
      });
  });
})();