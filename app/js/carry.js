// Функция для осуществления каррирования или
// шейнфинкелизации. В память о русском математике
// Моисее Исаевиче Шейнфинкеле.
// Взята полностью из книги Стояна Стефанова «JavaScript Шаблоны».
function carry(fn) {
  var slice = Array.prototype.slice,
      storedArguments = slice.call(arguments, 1);

  return function () {
    var newArguments = slice.call(arguments),
        args = storedArguments.concat(newArguments);

    return fn.apply(null, args);
  }
}