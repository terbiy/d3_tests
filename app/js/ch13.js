(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 600,
        HEIGHT = 600;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  var position = function (time) {
    var a = 80,
        b = 40,
        c = 1,
        d = 80;

    return {
      x: Math.cos(a * time) - Math.pow(Math.cos(b * time), 3),
      y: Math.sin(c * time) - Math.pow(Math.sin(d * time), 3)
    };
  };

  var timeScale = (
    d3.scale
      .linear()
      .domain([500, 25000])
      .range([0, 2 * Math.PI])
  );
  var x = (
    d3.scale
      .linear()
      .domain([-2, 2])
      .range([100, WIDTH - 100])
  );
  var y = (
    d3.scale
      .linear()
      .domain([-2, 2])
      .range([HEIGHT - 100, 100])
  );
  var brush = (
    svg
      .append("circle")
      .attr({
        r: 4
      })
  );
  var previous = position(0);

  var step = function (time) {
    if (time > timeScale.domain()[1]) {
      return true;
    }

    var t = timeScale(time),
        pos = position(t);

    brush
      .attr({
        cx: x(pos.x),
        cy: y(pos.y)
      });

    svg
      .append("line")
      .attr({
        x1: x(previous.x),
        y1: y(previous.y),
        x2: x(pos.x),
        y2: y(pos.y),
        stroke: "steelblue",
        "stroke-width": 1.3
      });

    previous = pos;
  };

  var timer = d3.timer(step);
})();