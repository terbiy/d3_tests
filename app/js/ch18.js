new Common(["carry", "uniques", "visualModifications"], function (common) {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = common.carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 1024;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  d3.json("./data/karma_matrix.json", function (data) {
    // Работа с круговой диаграммой.
    var filtered = (
      data
        .filter(function (datum) {
          return datum.to == "HairyFotr";
        })
    );
    var perNick = (
      common
        .binPerNick(
          filtered,
          function (datum) {
            return datum.from
          }
        )
    );
    var pie = (
      d3.layout
        .pie()
        .value(function (datum) {
          return datum.length;
        })(perNick)
    );

    var arc = (
      d3.svg
        .arc()
        .outerRadius(150)
        .startAngle(function (datum) {
          return datum.startAngle;
        })
        .endAngle(function (datum) {
          return datum.endAngle;
        })
    );

    common.fixateColors(data);

    var slice = (
      svg
        .selectAll(".slice")
        .data(pie)
        .enter()
        .append("g")
        .attr({
          transform: common.getTranslationString(300, 300)
        })
    );

    slice
      .append("path")
      .attr({
        d: arc,
        fill: function (datum) {
          return common.color(datum.data[0].from);
        }
      });

    slice
      .call(
        common
          .arcLabels(function (datum) {
              return datum.data[0].from;
            },
            arc.outerRadius()
          )
      );

  });
});
