(function () {
  "use strict";
  function numberToUnits (units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 800,
        HEIGHT = 600,
        MARGIN = 20;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  var x = (
    d3.scale
      .linear()
      .domain([0, 100])
      .range([MARGIN, WIDTH - MARGIN])
  );

  var axes = [
    d3.svg
      .axis()
      .scale(x),
    d3.svg
      .axis()
      .scale(x)
      .ticks(5),
    /* Функциональность отсутствует в текущей версии D3.
    d3.svg
      .axis()
      .scale(x)
      .tickSubdivide(3)
      .tickSize(10, 5, 10),
    */
    d3.svg
      .axis()
      .scale(x)
      .tickValues([0, 20, 50, 70, 100])
      .tickFormat(function (data, index) {
        return ["a", "e", "i", "o", "u"][index];
      })
      .orient("top")
  ]
  axes.forEach(function (axisBlank, index) {
    var axis = (
      svg
        .append("g")
        .classed("axis", true)
        .classed("red", index % 2 === 0)
        .attr({
          transform: `translate(0, ${40 + 50 * index})`
        })
        .call(axisBlank)
    );
  
    axis
      .selectAll("path")
      .attr({
        fill: "none",
        stroke: "black",
        "stroke-width": 1
      });
  
    axis
      .selectAll("line")
      .attr({
        fill: "none", 
        stroke: "black",
        "stroke-width": 0.3
      });
  });
})();