new Common(["carry", "uniques", "visualModifications"], function (common) {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = common.carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 1024;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  d3.json("./data/karma_matrix.json", function (data) {
    var time = (
      d3.time
        .format("%Y-%m-%d %H:%M:%S")
    );

    var extent = (
      d3.extent(
        data.map(function (datum) {
          return time.parse(datum.time);
        })
      );
    );

    var timeBins = (
      d3.time
        .days(extent[0], extent[1], 12)
    );

    var perNick = (
      common.binPerNick(data, function (datum) {
        datum.to;
      })
    );

    var timeBinned = perNick.map(function (nickLayer) {
      return {
        to: nickLayer.to,
        values: (
          d3.layout
            .histogram()
            .bins(timeBins)
            .value(function (datum) {
              return time.parse(datum.time);
            })(nickLayer)
        );
      };
    });

    var layers = (
      d3.layout
        .stack()
        .order("inside-out")
        .offset("wiggle")
        .values(function (datum) {
          return datum.values;
        })(timeBinned)
    );

    var margins = {
      top: 220,
      right: 50,
      bottom: 0,
      left: 50
    };
    var x = (
      d3.time
        .scale()
        .domain(extent)
        .range([margins.left, WIDTH - margins.right])
    );

    var y = (
      d3.scale
        .linear()
        .domain([
          0,
          d3.max(
            layers,
            function (layer) {
              return (
                d3.max(
                  layer.values,
                  function (datum) {
                    return datum.y0 + datum.y;
                  }
                )
              );
            }
          )
        ])
    );

  });
});
