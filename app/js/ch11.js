(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 1800,
        HEIGHT = 1200;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  var projection = (
    d3.geo
      .equirectangular()
      .center([8, 56])
      .scale(800)
  );

  queue()
    .defer(d3.json, "./data/geo/water.json")
    .defer(d3.json, "./data/geo/land.json")
    .defer(d3.json, "./data/geo/cultural.json")
    .await(draw);

  function addToMap(collection, key) {
    return (
      svg
        .append("g")
        .selectAll("path")
        .data(
          topojson
            .object(collection, collection.objects[key])
            .geometries
        )
        .enter()
        .append("path")
        .attr({
          d: (
            d3.geo
              .path()
              .projection(projection)
          )
        })
    );
  }

  function addAirports() {
    queue()
      .defer(d3.text, "./data/geo/airports.dat")
      .defer(d3.text, "./data/geo/routes.dat")
      .await(drawAirports);
  }

  function drawAirports (error, _airports, _routes) {
    var airports = {},
        routes = {};

    d3.csv
      .parseRows(_airports)
      .forEach(function (airport) {
        var id = airport[0];

        airports[id] = {
          lat: airport[6],
          lon: airport[7]
        };
        // console.log(airport)
        // console.log(airports[id]);
      });

    d3.csv
      .parseRows(_routes)
      .forEach(function (route) {
        var from_airport = route[3];

        if(!routes[from_airport]) {
          routes[from_airport] = [];
        }

        routes[from_airport].push({
          to: route[5],
          from: from_airport,
          stops: route[7]
        });
      });

    var route_N = (
      d3.values(routes)
        .map(function (routes) {
          return routes.length;
        })
    );
    var r = (
      d3.scale
        .linear()
        .domain(
          d3.extent(route_N)
        )
        .range([2, 14])
    );

    svg
      .append("g")
      .selectAll("circle")
      .data(
        d3.keys(airports)
      )
      .enter()
      .append("circle")
      .attr({
        transform: function (id) {
          var airport = airports[id];
          return `translate(${projection([airport.lon, airport.lat])})`;
        },
        r: function (id) {
          return routes[id] ? r(routes[id].length) : 1;
        }
      })
      .classed("airport", true);
  }

  function draw(error, water, land, cultural) {
    addToMap(water, "ne_50m_ocean")
      .classed("ocean", true);
    addToMap(land, "ne_50m_land")
      .classed("land", true)
    addToMap(water, "ne_50m_rivers_lake_centerlines")
      .classed("river", true)
    addToMap(cultural, "ne_50m_admin_0_boundary_lines_land")
      .classed("boundary", true)
    addToMap(cultural, "ne_10m_urban_areas")
      .classed("urban", true)
    addAirports();
  };
})();