(function () {
  "use strict";
  function numberToUnits (units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 768,
        RINGS = 15;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  var colors = (
    d3.scale
      .category20b()
  );

  var angle = (
    d3.scale
      .linear()
      .domain([0, 20])
      .range([0, 2 * Math.PI])
  );

  var arc = (
    d3.svg
      .arc()
      .innerRadius(function (datum) {
        return 50 * datum / RINGS;
      })
      .outerRadius(function (datum) {
        return 50 + 50 * datum / RINGS;
      })
      .startAngle(function (datum, index, j) {
        return angle(j);
      })
      .endAngle(function (datum, index, j) {
        return angle(j + 1);
      })
  );

  var shade = {
    darker: function (datum, j) {
      return (
        d3.rgb(colors(j))
          .darker(datum / RINGS)
      );
    },
    brighter: function (datum, j) {
      return (
        d3.rgb(colors(j))
          .brighter(datum / RINGS)
      );
    }
  };

  [
    [100, 100, shade.darker],
    [300, 100, shade.brighter]
  ].forEach(function (conf) {
    svg
      .append("g")
      .attr({
        transform: `translate(${conf[0]}, ${conf[1]})`
      })
      .selectAll("g")
      .data(colors.range())
      .enter()
      .append("g")
      .selectAll("path")
      .data(function (datum) {
        return (
          d3.range(0, RINGS)
        );
      })
      .enter()
      .append("path")
      .attr({
        d: arc,
        fill: function (datum, index, j) {
          return conf[2](datum, j);
        }
      });
  });
})();