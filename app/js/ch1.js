(function () {
  'use strict';

  // Параметри диаграммы.
  const WIDTH = 900,
        HEIGHT = 300,
        PAD = 20,
        LEFT_PAD = 100;

  var x = d3.scale.ordinal().rangeRoundBands([LEFT_PAD, WIDTH - PAD], 0.1),
      y = d3.scale.linear().range([HEIGHT - PAD, PAD]);

  var xAxis = d3.svg.axis().scale(x).orient("bottom"),
      yAxis = d3.svg.axis().scale(y).orient("left");

  var svg = d3.select("#graph").append("svg").attr("width", WIDTH).attr("height", HEIGHT);

  d3.json("./data/histogram-hours.json", function (rawData) {
    var data = d3.keys(rawData).map(function (key) {
      return {
        bucket: Number(key),
        number: rawData[key]
      };
    });

    // Натройка абсциссы.
    var buckets = data.map(
      function (datum) {
        return datum.bucket;
      }
    );
    x.domain(buckets);

    // Настройка ординаты.
    var maximumYValue = d3.max(
      data,
      function (datum) {
        return datum.number;
      }
    );
    y.domain([
      0,
      maximumYValue
    ]);

    // Прикрепление осей к графику.
    svg.append("g")
       .attr("class", "axis")
       .attr("transform", `translate(0, ${HEIGHT - PAD})`)
       .call(xAxis);
    
    svg.append("g")
       .attr("class", "axis")
       .attr("transform", `translate(${LEFT_PAD - PAD}, 0)`)
       .call(yAxis);

    svg.selectAll("rect")
       .data(data)
       .enter()
       .append("rect")
       .attr("class", "bar")
       .attr("x", function (datum) {
         return x(datum.bucket);
       })
       .attr("width", x.rangeBand())
       // .attr("y", function (datum) {
       //   return y(datum.number) + PAD;
       // })
       // .attr("height", function (datum) {
       //   return HEIGHT - 4 * PAD - y(datum.number);
       // })
       // .transition()
       // .delay(function (datum) {
       //   return datum.bucket * 20;
       // })
       // .duration(800)
       .attr("y", function (datum) {
         return y(datum.number);
       })
       .attr("height", function (datum) {
         return HEIGHT - PAD - y(datum.number);
       });
  });
})();
