(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 1200,
        HEIGHT = 450;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  svg
    .append("image")
    .attr({
      "xlink:href": "./images/parallax_base.png",
      width: WIDTH,
      height: HEIGHT
    });

  const SCREEN_WIDTH = 900;
  var lines = d3.range(SCREEN_WIDTH / 6),
      x = (
        d3.scale
          .ordinal()
          .domain(lines)
          .rangeBands([0, SCREEN_WIDTH])
      );

  svg
    .append("g")
    .selectAll("line")
    .data(lines)
    .enter()
    .append("line")
    .style({
      "shape-rendering": "crispEdges"
    })
    .attr({
      stroke: "black",
      "stroke-width": x.rangeBand() - 1,
      x1: function (datum) { return x(datum); },
      y1: 0,
      x2: function (datum) { return x(datum); },
      y1: HEIGHT,
    });

  var drag = (
    d3.behavior
      .drag()
      .origin(Object)
      .on("drag", function () {
        d3.select(this)
          .attr({
            transform: `translate(${d3.event.x}, 0)`
          })
          .datum({
            x: d3.event.x,
            y: 0
          })
      })
  );

  svg
    .select("g")
    .datum({
      x: 0,
      y: 0
    })
    .call(drag);
})();