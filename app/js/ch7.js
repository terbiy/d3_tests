(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 900,
        HEIGHT = 900;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  const POINTS_AMOUNT = 8000;

  // Задание координат спирали.
  var spiral = function (maxNumber) {
    var directions = {
      up: [0, -1],
      left: [-1, 0],
      down: [0, 1],
      right: [1, 0]
    };
    var directionNames = [
      "up",
      "left",
      "down",
      "right"
    ];

    var x = 0,
        y = 0,
        min = [0, 0],
        max = [0, 0],
        currentDirection = [0, 0],
        direction = 0;


    // Переключение между одним из 4-х направлений.
    var upgradeDirection = function () {
      direction = (direction + 1) % 4;
    }

    // Массив точек, образующих заготовку для скатерти Улама.
    var spiral = [];

    d3.range(1, maxNumber).forEach(function (index) {
      spiral.push({
        x: x,
        y: y,
        number: index
      });

      // Осуществить шаг.
      var currentDirectionName = directionNames[direction];
      currentDirection = directions[currentDirectionName];

      x += currentDirection[0];
      y += currentDirection[1];

      // Перестроить направление, если это необходимо.
      if (x < min[0]) {
        upgradeDirection();
        min[0] = x;
      }

      if (x > max[0]) {
        upgradeDirection();
        max[0] = x;
      }

      if (y < min[1]) {
        upgradeDirection();
        min[1] = y;
      }

      if (y > max[1]) {
        upgradeDirection();
        max[1] = y;
      }
    });

    return spiral;
  };

  var dot = (
    d3.svg
      .symbol()
      .type("square")
      .size(4)
  );

  // Смещение по горизонтали и вертикали.
  const CENTER = WIDTH / 2;
  var x = function (x, factor) {
        return CENTER + factor * x;
      },
      y = function (y, factor) {
        return CENTER + factor * y;
      };

  d3.text(
    "./data/primes-to-100k.txt",
    function (error, data) {
      var primes = (
        data
          .split("\n")
          .slice(0, POINTS_AMOUNT)
          .map(Number)
      );

      // Фильтрация массива заготовки для того, чтобы получилась
      // скатерть Улана.
      var sequence = (
        spiral(d3.max(primes))
          .filter(function (dot) {
            // Третий аргумент true передаётся для запуска ускоренного
            // бинарного поиска.
            return _.indexOf(primes, dot["number"], true) > -1;
          })
      );


      const IMAGE_SCALE_FACTOR = 3,
            REGION_SIZE = 8;
      // Расчёт содержимого регионов.
      var regions = (
        d3.nest()
          // Разделение на регионы по оси x.
          .key(function (datum) {
            return Math.floor(datum["x"] / REGION_SIZE);
          })
          // Разделение на регионы по оси y.
          .key(function (datum) {
            return Math.floor(datum["y"] / REGION_SIZE);
          })
          // Количество элементов в каждом регионе.
          .rollup(function (datum) {
            return datum.length;
          })
          .map(sequence)
      );

      // Слить значение количеств точек в каждом из регионов в единый массив.
      var values = (
        d3.merge(
          d3.keys(regions)
            .map(function (_x) {
              return d3.values(regions[_x]);
            })
        )
      );

      // Вычисление:
      // - Медианы
      // - Крайних значений
      // - Число возможных оттенков. Или нет.
      var median = d3.median(values),
          extent = d3.extent(values),
          shades = (extent[1] - extent[0]) / 1.5;

      d3.keys(regions)
        // Для каждого региона по оси x.
        .forEach(function (_x) {
          d3.keys(regions[_x])
            // Для каждого региона по оси y.
            .forEach(function (_y) {
              const RED = "#e23c22",
                    GREEN = "#497c36";
              var color;

              // В зависимости от отношения числа элементов к медиане
              // выбирается цвет региона.
              if (regions[_x][_y] > median) {
                color = (
                  d3.rgb(GREEN)
                    .brighter(regions[_x][_y] / shades)
                );
              } else {
                color = (
                  d3.rgb(RED)
                    .darker(regions[_x][_y] / shades)
                );
              }

              // Этот момент заставил меня немного подзадуматься, так как
              // я, видимо, несколько механически перепечатывал код программы,
              // несмотря на все попытки его улучшить.
              // Функции x и y объявлены выше.
              // _x, _y — параметры, передаваемые в обе функции forEach.
              // Никакой хитрой внутренней передачи тут не происходит.
              svg
                .append("rect")
                .attr({
                  x: x(_x, IMAGE_SCALE_FACTOR * REGION_SIZE),
                  y: y(_y, IMAGE_SCALE_FACTOR * REGION_SIZE),
                  width: IMAGE_SCALE_FACTOR * REGION_SIZE,
                  height: IMAGE_SCALE_FACTOR * REGION_SIZE
                })
                .style({
                  fill: color,
                  "fill-opacity": 1
                });
            });
        });

      // Добавление точек.
      svg
        .selectAll("path")
        .data(sequence)
        .enter()
        .append("path")
        .attr({
          transform: function (datum) {
            return `translate(${x(datum["x"], IMAGE_SCALE_FACTOR)}, ${y(datum["y"], IMAGE_SCALE_FACTOR)})`;
          },
          "shape-rendering": "crispEdges",
          d: dot
        });
    }
  );
})();
