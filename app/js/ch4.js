(function () {
  "use strict";
  function numberToUnits (units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 768,
        MARGIN = 10;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  var group = (
    svg
      .append("g")
      .attr({
        transform: `translate(${MARGIN}, ${MARGIN})`
      })
  );

  var sine = (
    d3.range(0, 20)
      .map(function (factor) {
        return [
          0.5 * factor * Math.PI,
          Math.cos(0.5 * factor * Math.PI)
        ];
      })
  );

  var x = (
    d3.scale
      .linear()
      .range([0, WIDTH / 2 - MARGIN])
      .domain(
        d3.extent(
          sine,
          function (d) {
            return d[0];
          }
        )
      )
  );

  var y = (
    d3.scale
      .linear()
      .range([HEIGHT / 2 - MARGIN, 0])
      .domain([-1, 1])
  );

  var line = (
    d3.svg
      .line()
      .x(function (dot) {
        return x(dot[0]);
      })
      .y(function (dot) {
        return y(dot[1]);
      })
  );

  group.append("path")
    .datum(sine)
    .attr("d", line.interpolate("basis"))
    .attr({
      stroke: "steelblue",
      "stroke-width": 2,
      fill: "none"
    });

  // Заполненная фигура.
  var group2 = (
    svg
      .append("g")
      .attr({
        transform: `translate(${WIDTH / 2 + MARGIN}, ${MARGIN})`
      })
  );

  var area = (
    d3.svg
      .area()
      .x(function (dot) {
        return x(dot[0]);
      })
      .y0(HEIGHT / 2)
      .y1(function (dot) {
        return y(dot[1]);
      })
      .interpolate("basis")
  );

  group2
    .append("path")
    .datum(sine)
    .attr({
      d: area
    })
    .attr({
      fill: "steelblue",
      "fill-opacity": 0.4
    });

  group2
    .append("path")
    .datum(sine)
    .attr({
      d: line.interpolate("basis")
    })
    .attr({
      stroke: "steelblue",
      "stroke-width": 2,
      fill: "none"
    });

  // Работа с кривыми.
  var arc = (
    d3.svg
      .arc()
  );

  var group3 = (
    svg
      .append("g")
      .attr({
        transform: `translate(${MARGIN}, ${HEIGHT / 2 + MARGIN})`
      })
  );

  group3
    .append("path")
    .attr({
      d: arc({
        outerRadius: 100,
        innerRadius: 49,
        startAngle: -0.25 * Math.PI,
        endAngle: 0.25 * Math.PI
      }),
      transform: `translate(150, 150)`,
      fill: "lightslategray"
    });

  var group4 = (
    svg
      .append("g")
      .attr({
        transform: `translate(${MARGIN}, ${HEIGHT / 2 + MARGIN})`
      })
  );

  group4
    .append("path")
    .attr({
      d: arc({
        outerRadius: 50,
        innerRadius: 24,
        startAngle: -0.25 * Math.PI,
        endAngle: 0.25 * Math.PI
      }),
      transform: `translate(150, 150)`,
      fill: "orange"
    });

  var group5 = (
    svg
      .append("g")
      .attr({
        transform: `translate(${MARGIN}, ${HEIGHT / 2 + MARGIN})`
      })
  );

  group5
    .append("path")
    .attr({
      d: arc({
        outerRadius: 25,
        innerRadius: 0,
        startAngle: -0.25 * Math.PI,
        endAngle: 0.25 * Math.PI
      }),
      transform: `translate(150, 150)`,
      fill: "lightslategray"
    });

  // Подписи данных.
  var symbols = (
    d3.svg
      .symbol()
      .type(function (datum, index) {
        if (datum[1] > 0) {
          return "triangle-down";
        } else {
          return "triangle-up";
        }
      })
      .size(function (datum, index) {
        if (index % 2) {
          return 0;
        } else {
          return 64;
        }
      })
  );

  group2
    .selectAll("path")
    .data(sine)
    .enter()
    .append("path")
    .attr({
      d: symbols,
      transform: function (datum) {
        return `translate(${x(datum[0])}, ${y(datum[1])})`
      },
      stroke: "steelblue",
      "stroke-width": 1,
      fill: "white"
    });

  // То ли хорды, то ли ещё что-то.
  group3
    .append("g")
    .selectAll("path")
    .data([{
      source: {
        radius: 90,
        startAngle: 0.45 * Math.PI,
        endAngle: 0.55 * Math.PI
      },
      target: {
        radius: 50,
        startAngle: 0.6 * Math.PI,
        endAngle: 2.4 * Math.PI
      }
    }])
    .enter()
    .append("path")
    .attr({
      d: d3.svg.chord(),
      transform: `translate(250, 150)`
    });

  // Сделаем хордовую диаграмму.
  var data = d3.zip(
    d3.range(0, 12),
    d3.shuffle(d3.range(0, 12))
  );
  const DATA_LENGTH = data.length;

  var colors = [
    "linen",
    "lightslategray",
    "lavender",
    "honeydew",
    "gainsboro"
  ];

  var chord = (
    d3.svg
      .chord()
      .source(function (datum) {
        return datum[0];
      })
      .target(function (datum) {
        return datum[1];
      })
      .radius(150)
      .startAngle(function (datum) {
        return -2 * Math.PI * (1 / DATA_LENGTH) * datum;
      })
      .endAngle(function (datum) {
        return -2 * Math.PI * (1 / DATA_LENGTH) * ((datum - 1) % DATA_LENGTH);
      })
  );

  group3
    .append("g")
    .attr({
      transform: `translate(600, 180)`
    })
    .selectAll("path")
    .data(data)
    .enter()
    .append("path")
    .attr({
      d: chord,
      fill: function (datum, index) {
        return colors[index % colors.length];
      },
      stroke: function (datum, index) {
        return colors[(index + 1) % colors.length];
      }
    });

  // Диагонали.
  var group6 = (
    svg
      .append('g')
      .attr({
        transform: `translate(${WIDTH / 2}, ${HEIGHT / 2})`
      })
  );

  var moustache = [
    {source: {x: 250, y: 100}, target: {x: 500, y: 90}},
    {source: {x: 500, y: 90}, target: {x: 250, y: 120}},
    {source: {x: 250, y: 120}, target: {x: 0, y: 90}},
    {source: {x: 0, y: 90}, target: {x: 250, y: 100}},
    {source: {x: 500, y: 90}, target: {x: 490, y: 80}},
    {source: {x: 0, y: 90}, target: {x: 10, y: 80}}
  ];

  group6.selectAll('path')
    .data(moustache)
    .enter()
    .append('path')
    .attr({
      d: d3.svg.diagonal(),
      stroke: 'black',
      fill: 'none'
    });
})();