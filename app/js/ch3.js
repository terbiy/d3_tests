(function () {
  "use strict";
  function numberToUnits (units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 768;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  svg
    .append("text")
    .text("A picture!")
    .attr({
      x: 10,
      y: 150,
      "text-anchor": "start"
    });

  svg
    .append("line")
    .attr({
      x1: 10,
      y1: 10,
      x2: 100,
      y2: 100,
      stroke: "steelblue",
      "stroke-width": 3
    });

  svg
    .append("rect")
    .attr({
      x: 200,
      y: 50,
      width: 300,
      height: 400
    });

  svg
    .select("rect")
    .attr({
      stroke: "green",
      "stroke-width": 0.5,
      fill: "white",
      rx: 20,
      ry: 20
    });

  svg
    .append("circle")
    .attr({
      cx: 350,
      cy: 250,
      r: 100,
      fill: "green",
      "fill-opacity": "0.5",
      stroke: "steelblue",
      "stroke-width": 2
    });

  svg
    .append("ellipse")
    .attr({
      cx: 350,
      cy: 250,
      rx: 150,
      ry: 70,
      fill: "green",
      "fill-opacity": 0.3,
      stroke: "steelblue",
      "stroke-width": 0.7
    });

  svg
    .append("ellipse")
    .attr({
      cx: 350,
      cy: 250,
      rx: 20,
      ry: 70
    });


  // Манипуляции с эллипсами и кругами.
  const SCALE_FACTOR = 1;
  const ANGLE = -45;
  var transformation = `translate(150, 0) scale(${SCALE_FACTOR}) rotate(${ANGLE}, ${350 / SCALE_FACTOR}, ${250 / SCALE_FACTOR}) skewY(20)`;
  console.log(transformation);
  svg
    .selectAll("ellipse, circle")
    .attr({
      transform: transformation
    });

  svg
    .append("path")
    .attr({
      d: "M 100 100 L 300 100 L 200 300 z",
      stroke: "black",
      "stroke-width": 2,
      fill: "red",
      "fill-opacity": 0.7
    });
})();