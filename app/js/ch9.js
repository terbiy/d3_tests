(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 800,
        HEIGHT = 600;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  var data = d3.range(30),
      colors = d3.scale.category10();

  var points = (
    d3.scale
      .ordinal()
      .domain(data)
      .rangePoints([0, HEIGHT], 1.0)
  );

  var bands = (
    d3.scale
      .ordinal()
      .domain(data)
      .rangeBands([0, WIDTH], 0.2)
  );

  svg
    .selectAll("path")
    .data(data)
    .enter()
    .append("path")
    .attr({
      d: (
        d3.svg
          .symbol()
          .type("cirlce")
          .size(function (datum) {
            return 10 * datum;
          })
      ),
      transform: function (datum) {
        return `translate(${WIDTH / 2}, ${points(datum)})`
      }
    })
    .style({
      fill: function (datum) {
        return colors(datum);
      }
    })

  svg
    .selectAll("rect")
    .data(data)
    .enter()
    .append("rect")
    .attr({
      x: function (datum) {
        return bands(datum);
      },
      y: function (datum) {
        return (HEIGHT - 10 * datum) / 2;
      },
      width: bands.rangeBand(),
      height: function (datum) {
        return 10 * datum;
      },
      "shape-rendering": "crispEdges"
    })
    .style({
      fill: function (datum) {
        return colors(datum);
      }
    });

})();