(function () {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = carry(numberToUnits, "px");

  const WIDTH = 800,
        HEIGHT = 600;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .style("width", numberToPx(WIDTH))
      .style("height", numberToPx(HEIGHT))
  );

  // Функция, носящая имя Вейерштрасса.
  // Даёт координаты в сочетании с x выдающие вогнутую линию
  // с примесью дабстепа, зависящей от размера константы a.
  var weierstrass = function (x) {
    const a = 0.5,
          b = (1 + 3 * Math.PI / 2) / a;

    return (
      d3.sum(
        d3.range(100)
          .map(function (power) {
            // Неплохо так в сотую степень возводить.
            return Math.pow(a, power) * Math.cos(Math.pow(b, power) * Math.PI * x);
          })
      )
    );
  };

  var drawOne = function (line) {
    return (
      svg
        .append("path")
        .datum(data)
        .attr({
          d: line
        })
        .style({
          "stroke-width": 2,
          fill: "none"
        })
    );
  }

  // Сформированный для задачи массив данных.
  var data = (
    d3.range(-300, 300)
      .map(function (number) {
        return number / 200;
      })
  );

  // Крайние значения графика по оси y.
  var extent = (
    d3.extent(data.map(weierstrass))
  );

  var colors = d3.scale.category10();

  // Установка соответствия между областью определения и протяжённостью по оси x.
  var x = (
    d3.scale
      .linear()
      .domain(
        d3.extent(data)
      )
      .range([0, WIDTH])
  );

  // Установка соответствия между областью определения и протяжённостью по оси y.
  var linear = (
    d3.scale
      .linear()
      .domain(extent)
      .range([HEIGHT / 4, 0])
  );

  var lines = [];

  // Создание заготовки для определения точек линии.
  lines[0] = (
    d3.svg
      .line()
      .x(x)
      .y(function (datum, index) {
        return linear(weierstrass(datum));
      })
  );

  var initialLine = lines[0];

  // Отрисовка первой линии.
  drawOne(initialLine)
    .attr({
      transform: `translate(0, ${HEIGHT / 16})`
    })
    .style({
      stroke: function (datum) { return colors(0);}
    });

  // Отрисовка второй линии.
  var identity = (
    d3.scale
      .identity()
      .domain(extent)
  );

  lines[1] = (
    initialLine
      .y(function (datum) {
        return identity(weierstrass(datum));
      })
  );

  drawOne(lines[1])
    .attr({
      transform: `translate(0, ${HEIGHT / 12})`
    })
    .style({
      stroke: colors(1)
    });

  // Отрисовка третьей линии.
  var power = (
    d3.scale
      .pow()
      .exponent(0.3)
      .domain(extent)
      .range([HEIGHT / 2, 0])
  );

  lines[2] = (
    initialLine
      .y(function (datum) {
        return power(weierstrass(datum));
      })
  );

  drawOne(lines[2])
    .attr({
      transform: `translate(0, ${HEIGHT / 8})`
    })
    .style({
      stroke: colors(2)
    });

  // Отрисовка четвёртой линии.
  var log = (
    d3.scale
      .log()
      .domain(
        // Изъятие отрицательных значений для описания области значения.
        d3.extent(
          data
            .filter(function (datum) {
              return datum > 0 ? datum : 0;
            })
        )
      )
      .range([0, WIDTH])
  );

  lines[3] = (
    initialLine
      .x(function (datum) {
        return datum > 0 ? log(datum) : 0;
      })
      .y(function (datum) {
        return linear(weierstrass(datum));
      })
  );

  drawOne(lines[3])
    .attr({
      transform: `translate(0, ${HEIGHT / 4})`
    })
    .style({
      stroke: colors(3)
    });


  // Отрисовка пятой линии.
  var quantize = (
    d3.scale
      .quantize()
      .domain(extent)
      .range(
        d3.range(-1, 2, 0.5)
          .map(function (datum) {
            return datum * 100;
          })
      )
  );

  lines[4] = (
    initialLine
      .x(x)
      .y(function (datum) {
        return quantize(weierstrass(datum));
      })
  );

  const OFFSET = 100;
  drawOne(lines[4])
    .attr({
      transform: `translate(0, ${HEIGHT / 2 + OFFSET})`
    })
    .style({
      stroke: colors(4)
    });

  // Отрисовка шестой линии.
  var threshold = (
    d3.scale
      .threshold()
      .domain([-1, 0, 1])
      .range([-50, 0, 50, 100])
  );

  lines[5] = (
    initialLine
      .x(x)
      .y(function (datum) {
        return threshold(weierstrass(datum));
      })
  );

  drawOne(lines[5])
    .attr({
      transform: `translate(0, ${HEIGHT / 2 + 2 * OFFSET})`
    })
    .style({
      stroke: colors(5)
    });
})();