new Common(["carry", "uniques", "visualModifications"], function (common) {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = common.carry(numberToUnits, "px");

  const WIDTH = 1024,
        HEIGHT = 1024;

  var svg = (
    d3.select("#graph")
      .append("svg")
      .attr({
        width: numberToPx(WIDTH),
        height: numberToPx(HEIGHT)
      })
  );

  d3.json("./data/karma_matrix.json", function (data) {
    var nickId = common.nickId(data, function (datum) {
      return datum.to;
    });
    var histogram = (
      d3.layout
        .histogram()
        .bins(nickId.range())
        .value(function (datum) {
          return nickId(datum.to);
        })(data)
    );
    var margins = {
      top: 10,
      right: 40,
      bottom: 100,
      left: 50
    };
    var x = (
      d3.scale
        .linear()
        .domain([
          0,
          d3.max(histogram, function (datum) {
            return datum.x;
          })
        ])
        .range([
          margins.left,
          WIDTH - margins.right
        ])
    );
    var y = (
      d3.scale
        .log()
        .domain([
          1,
          d3.max(histogram, function (datum) {
            return datum.y;
          })
        ])
        .range([
          HEIGHT - margins.bottom,
          margins.top
        ])
    );
    var yAxis = (
      d3.svg
        .axis()
        .scale(y)
        .tickFormat(
          d3.format("f")
        )
        .orient("left")
    );

    svg
      .append("g")
      .classed("axis", true)
      .attr({
        transform: common.getTranslationString(50, 0)
      })
      .call(yAxis);

    var bar =(
      svg
        .selectAll(".bar")
        .data(histogram)
        .enter()
        .append("g")
        .classed("bar", true)
        .attr({
          transform: function (datum) {
            return common.getTranslationString(x(datum.x), y(datum.y));
          }
        })
    );

    bar
      .append("rect")
      .attr({
        x: 1,
        width: x(histogram[0].dx) - margins.left - 2,
        height: function (datum) {
          return HEIGHT - margins.bottom - y(datum.y);
        }
      });

    bar
      .append("text")
      .text(function (datum) {
        return datum[0].to;
      })
      .attr({
        transform: function (datum) {
          var barHeight = HEIGHT - margins.bottom - y(datum.y);

          return `${common.getTranslationString(0, barHeight + 7)} rotate(60)`;
        }
      })
  });
});
