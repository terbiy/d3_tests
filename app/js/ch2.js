(function () {
  var Data;

  var table = (
    d3.select("#graph")
      .append("table")
      .attr("class", "table")
  );

  var thead = table.append("thead"),
      tbody = table.append("tbody");

  var ascending = function (firstCompared, secondCompared) {
    return d3.ascending(firstCompared["Year first"], secondCompared["Year first"]);
  };

  var redraw = function () {
    var tr;
    tr = tbody.selectAll("tr").data(Data);
    tr.enter().append("tr");
    tr.exit().remove();
    tr.selectAll("td")
      .data(function (datum) {
        return d3.values(datum);
      })
      .enter()
      .append("td")
      .text(function (datum) {
        return datum;
      });

    tbody
      .selectAll("tr")
      .sort(ascending);
  };

  var reload = function () {
    d3.csv("./data/villains.csv", function (data) {
      Data = data;
      Data = Data.filter(function (datum) {
        return datum["Doctor actor"] === "Matt Smith";
      });
      redraw();
    });
  };

  reload();
})();