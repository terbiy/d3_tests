new Common(["carry", "analysis"], function (common) {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = common.carry(numberToUnits, "px");

  const MARGIN = 40,
        WIDTH = 800,
        HEIGHT = 300,
        HALF_HEIGHT = HEIGHT / 2;

  d3.csv("./data/salary.csv", function (data) {
    // TODO: изучить, можно ли средствами D3 получить
    // усреднённые значения для каждого года и использовать их.
    var mediumYearSalaries = (
      data.map(function (yearDatum) {
        var mediumYearSalary = 0,
            monthSalary = 0,
            monthesInYear = 0;

        for (var month in yearDatum) {
          if (yearDatum.hasOwnProperty(month) && month != "Год") {
            monthSalary = parseFloat(yearDatum[month], 10);

            if (!isNaN(monthSalary)) {
              mediumYearSalary += monthSalary;
              monthesInYear++;
            }
          }
        }

        mediumYearSalary /= monthesInYear;
        mediumYearSalary = parseFloat(mediumYearSalary.toFixed(2))

        return mediumYearSalary;
      })
    );

    drawSalaryValues(data, "#graph", mediumYearSalaries);
    drawSalaryDynamics(data, "#graph", mediumYearSalaries);

    d3.csv("./data/inflation.csv", function (data) {
      const SALARY_1990 = 303;
      var inflations = data,
          cumulativeInflation = 1;
      var cumulativeInflations = inflations.map(function (inflationDatum, index) {
        var inflation = parseFloat(inflationDatum["Инфляция"]) / 100 + 1;
        cumulativeInflation *= inflation;
        return cumulativeInflation;
      });
      var realMediumYearSalaries = mediumYearSalaries.map(function (mediumYearSalary, salaryIndex) {
        cumulativeInflation = cumulativeInflations[salaryIndex];
        return parseFloat((mediumYearSalary / cumulativeInflation * 1000).toFixed(3));
      });

      drawSalaryValues(data, "#graph", realMediumYearSalaries);
      drawSalaryDynamics(data, "#graph", realMediumYearSalaries);
    });
  });

  function drawSalaryDynamics(data, containerId, mediumYearSalaries) {
    var svg = (
      d3.select(containerId)
        .append("svg")
        .attr({
          width: numberToPx(WIDTH + 2 * MARGIN),
          height: numberToPx(HEIGHT + 2 * MARGIN),
          transform: `translate(${MARGIN}, ${MARGIN})`
        })
    );

    // Расчёт отношения зарплат текущего года к зарплате предыдущего.
    var previousYearSalary = mediumYearSalaries[0];
    var mediumYearSalariesGrowth = mediumYearSalaries.map(function (mediumYearSalary) {
      var growthRate = mediumYearSalary / previousYearSalary - 1;
      previousYearSalary = mediumYearSalary;
      return growthRate;
    });

    // Значения и ось x.
    var x = (
      d3.scale
        .ordinal()
        .rangeRoundBands([0, WIDTH], .2)
        .domain(
          data.map(
            function (yearDatum) {
              return yearDatum["Год"];
            }
          )
        )
    );
    var xAxis = (
      d3.svg
        .axis()
        .scale(x)
        .orient("bottom")
    );

    var y = (
      d3.scale
        .linear()
        .range([HEIGHT, 0])
        .domain(
          d3.extent(mediumYearSalariesGrowth)
        )
    );
    var yAxis = (
      d3.svg
        .axis()
        .scale(y)
        .orient("left")
        .ticks(10, "%")
        .tickSize(0)
        .tickPadding(6)
    );

    // Колонки значений.
    var bandAttributes = {
      "class": function (yearDatum, datumIndex) {
        var salaryGrowth = mediumYearSalariesGrowth[datumIndex],
            growthType = salaryGrowth >= 0 ? "positive" : "negative";
        return `bar bar-${growthType}`; 
      },
      x: function (yearDatum, datumIndex) {
        return x(yearDatum["Год"]);
      },
      y: function (yearDatum, datumIndex) {
        var salaryGrowth = mediumYearSalariesGrowth[datumIndex];
        // return y(Math.min(0, salaryGrowth));

        return y(Math.max(0, salaryGrowth));
      },
      width: function (yearDatum, datumIndex) {
        return x.rangeBand();
      },
      height: function (yearDatum, datumIndex) {
        var salaryGrowth = mediumYearSalariesGrowth[datumIndex],
            initialYCoordinate = y(0);
        return Math.abs(y(salaryGrowth) - initialYCoordinate);
      }
    }
    svg
      .selectAll(".bar")
      .data(data)
      .enter()
      .append("rect")
        .attr(bandAttributes);

    // Прикрепляем оси.
    var xAxisAttributes = {
      "class": "x axis",
      transform: `translate(0, ${y(0)})`
    };
    var yAxisAttributes = {
      "class": "y axis",
      transform: "translate(0, 0)"
    };
    svg
      .append("g")
      .attr(xAxisAttributes)
      .call(xAxis);
    svg
      .append("g")
      .attr(yAxisAttributes)
      .call(yAxis);
  }

  function drawSalaryValues(data, containerId, mediumYearSalaries) {
    var svg = (
      d3.select(containerId)
        .append("svg")
        .attr({
          width: numberToPx(WIDTH + 2 * MARGIN),
          height: numberToPx(HEIGHT + 2 * MARGIN),
          transform: `translate(${MARGIN}, ${MARGIN})`
        })
    );

    const SPACE_BETWEEN_BANDS = 0.2;
    var x = (
      d3.scale
        .ordinal()
        .rangeRoundBands([0, WIDTH], SPACE_BETWEEN_BANDS)
    );
    var xAxis = (
      d3.svg
        .axis()
        .scale(x)
        .orient("bottom")
    );
    var y = (
      d3.scale
        .linear()
        .range([HEIGHT, 0])
    );
    var yAxis = (
      d3.svg
        .axis()
        .scale(y)
        .orient("left")
    );

    // Установка значений по оси x.
    x.domain(
      data.map(
        function (yearDatum) {
          return yearDatum["Год"];
        }
      )
    );
    var maximumSalary = d3.max(mediumYearSalaries);
    y.domain(
      [
        0,
        maximumSalary
      ]
    );

    // Создание колонок диаграммы.
    var bandAttributes = {
      x: function (yearDatum) {
        return x(yearDatum["Год"]);
      },
      y: function (yearDatum, datumIndex) {
        var mediumYearSalary = mediumYearSalaries[datumIndex];
        return y(mediumYearSalary);
      },
      width: function (yearDatum) {
        return x.rangeBand();
      },
      height: function (yearDatum, datumIndex) {
        var mediumYearSalary = mediumYearSalaries[datumIndex];
        return HEIGHT - y(mediumYearSalary);
      },
      "class": function (yearDatum, datumIndex) {
        return `bar bar-${datumIndex}`;
      }
    }
    svg
      .selectAll(".bar")
      .data(data)
      .enter()
      .append("rect")
        .attr(bandAttributes)
        .on(
          "mouseover",
          function (yearDatum, datumIndex) {
            var sign = svg.select(`text.bar-${datumIndex}`);

            svg
              .selectAll(".highlighted")
              .classed("highlighted", false)
            svg
              .selectAll(".visible")
              .classed("visible", false)
            
            d3.select(this)
              .classed("highlighted", true);
            sign.classed("visible", true);
          }
        );

    // Подписи значений к диаграммам.
    var bandTextAttributes = {
      x: 200,
      y: 100,
      "class": function (yearDatum, datumIndex) {
        return `bar-text bar-${datumIndex}`;
      }
    };
    svg
      .selectAll(".bar-text")
      .data(data)
      .enter()
      .append("text")
        .attr(bandTextAttributes)
        .text(function (yearDatum, datumIndex) {
          var mediumYearSalary = mediumYearSalaries[datumIndex];
          return mediumYearSalary;
        });

    // Создание осей координат.
    // Ось x.
    var xAxisAttributes = {
      transform: `translate(0, ${HEIGHT})`
    };
    svg
      .append("g")
      .classed("x axis", true)
      .attr(xAxisAttributes)
      .call(xAxis);

    // Ось y.
    var yAxisTextAttributes = {
      x: 10,
      y: 10
    };
    svg.append("g")
      .classed("y axis", true)
      .call(yAxis)
      .append("text")
        .attr(yAxisTextAttributes)
        .text("Среднемесячная заработная плата в России в 1991–2016 годах");
  }
});