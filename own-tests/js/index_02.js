new d3Helpers(["svg", "data", "allCharts", "donutChart"], function (helpers) {
  var style = {
    margin: 30,
    width: 400,
    height: 200,
    innerRadius: 0.5,
    outerRadius: 1.0,
    colors: [
      "#3a121f",
      "#5c1d31",
      "#74243e",
      "#9b3053",
      "#c23c68",
      "#cf6286",
      "#db89a4",
      "#e7b0c2",
      "#f3d7e0",
      "#fffefe"
    ]
  };
  var data = [
    {abba1: 16},
    {abba2: 2},
    {abba3: 8},
    {abba4: 2},
    {abba13: 2}
  ];
  var someData = helpers.arrayOfObjectsToArrayOfKeyValues(data),
      donutChart = helpers.createDonutChart(someData, style),
      label = document.createElement("div");

  label.className = "label";

  var groupInfo = document.querySelector(".group-info");

  groupInfo.appendChild(donutChart.node());
  groupInfo.appendChild(label);

  var newData = [
    {abba1: 1},
    {abba2: 2},
    {abba3: 3},
    {abba4: 4},
    {abba5: 4},
    {abba6: 4},
    {nadaz: 4},
    {abba13: 6}
  ];
  groupInfo.addEventListener("click", function () {
    // Когда я создаю данные вне функции обратного вызова, я получаю интересный
    // побочный эффект. В случае, если в данных появляются новые ключи, то
    // последующее обновление значений приводит к моментальному обновлению
    // диаграммы (что для меня нежелательно). Если же новых ключей не
    // добавляется, то постоянной связи между диаграммой и данными как будто не
    // существует — диаграмма плавно обновляется.
    // История осталась для меня достаточно непонятной.
    helpers.getNewDataAndAnimateDonutChartUpdate(donutChart, newData)
      .then(function () {
        for (let datumIndex = 0; datumIndex < newData.length; datumIndex++) {
          let datum = newData[datumIndex];

          for (let property in datum) {
            if (datum.hasOwnProperty(property)) {
              datum[property] += Math.floor(Math.random() * 20) * Math.round(Math.random() * 2 - 1);
              datum[property] = Math.max(datum[property], 0);
            }
          }
        }
      });
    // someNewData.sort(function (a, b) {
    //   return b.value - a.value;
    // });
  });
});
// Мне необходимо сделать несколько элементов.

// Первый элемент представляет собой диаграмму-бублик.
// Диаграмма-бублик не должна каким-либо образом знать об особенностях
// передаваемых ей данных.
// Диаграмму-бублик я планирую создать при помощи d3.js и svg.

// Я планирую использовать диаграмму-бублик для того, чтобы отобразить структуру
// расходов населения.

// Второй элемент представляет собой легенду, содержащую информацию о
// диаграмме-бублике.
// Легенду я планирую создать при помощи d3.js, SVG и HTML, так как таким
// образом она станет более гибкой.
// Я предполагаю следующую структуру легенды:
// <section>
//   <dl class="category">
//     <dt class="category-color">
//       <место для вставки SVG>
//     </dt>
//     <dd class="category-description"></dd>
//   </dl>
//   ...
// </section>

// Третий элемент представляет собой переключатель между группами данных.
// Их число зависит от числа столбцов в таблице.

// По мере работы происходит следующее:
// 1) Загружаются данные и происходит разбивка по категориям населения.
// 2) Для первой категории населения создаётся диаграмма, отражающая группы
//    товаров.
// 3) Создаётся легенда, отражающая цветовую кодировку групп товаров.
// 4) Создаётся переключатель между категориями населения.

// При нажатии на переключатель и выборе другой категории населения
// параллельно происходит следующее:
// 1) Смена заголовка с анимацией исчезновения и появления.
// 2) Анимированное изменение размеров секторов диаграммы.
// 3) Выделение текущей точки на переключателе.
