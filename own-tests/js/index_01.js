new Common(["carry", "analysis"], function (common) {
  "use strict";
  function numberToUnits(units, number) {
    return `${number}${units}`;
  }
  // Каррируем функцию для случая пикселей.
  var numberToPx = common.carry(numberToUnits, "px");

  d3.csv("./data/spendings-structure.csv", function (error, spendingsCategories) {
    const populationCategories = (
      d3.keys(spendingsCategories[0])
        .filter(
          function (key) {
            return key !== "Категория";
          }
        )
    );

    const spendingsCategoriesNames = (
      spendingsCategories.map(function (categoryDatum) {
        return categoryDatum["Категория"];
      })
    );

    var chartsHolder = document.createDocumentFragment();

    var place = document.querySelector("#graph");

    for (
      var categoryIndex = 0,
          categoriesLength = populationCategories.length;
      categoryIndex < categoriesLength;
      categoryIndex++
    ) {
      let populationCategory = populationCategories[categoryIndex],
          dataForChart = [];

      for (
        var spendingsIndex = 0,
            spendingsLength = spendingsCategories.length;
        spendingsIndex < spendingsLength;
        spendingsIndex++
      ) {
        let spendingsCategory = spendingsCategories[spendingsIndex],
            spendingsCategoryName = spendingsCategory["Категория"],
            dataObject = {};

        dataObject[spendingsCategoryName] = spendingsCategory[populationCategory];
        dataObject["Название"] = spendingsCategoryName;
        dataObject["Число"] = spendingsCategory[populationCategory];

        dataForChart.push(dataObject);
      }

      let donutChart = drawDonutChart(dataForChart);

      // Вставить элемент на страницу.
      var chartsInfo = document.createElement("div"),
          chartHeader = document.createElement("h3");
      chartsInfo.className = "donut-chart";
      chartHeader.innerHTML = populationCategory;
      chartsInfo.appendChild(chartHeader);
      chartsInfo.appendChild(donutChart.node());
      chartsHolder.appendChild(chartsInfo);

      place.appendChild(chartsInfo);
    }

    var legend = drawLegend(spendingsCategoriesNames).node();
    place.appendChild(legend);
  });

  // Мне необходимо нарисовать легенду.
  // Для этого я планирую сделать отдельную функцию, в которую
  // вынесу создание данного элемента.
  function drawLegend (data) {
    const MARGIN = 20,
          WIDTH = 120,
          HEIGHT = 120,
          RADIUS = Math.min(WIDTH, HEIGHT) / 2;

    var svgElement = createSVG();

    var d3SVG = d3.select(svgElement);
    var svgAttributes = {
      "class": "legend",
      width: RADIUS * 10,
      height: RADIUS * 5
    };
    d3SVG.attr(svgAttributes);

    var colors = getColors();
    colors.domain(data);

    // Добавить группы для квадратов и надписей.
    var legendAttributes = {
      transform: function (categoryName, categoryIndex) {
        return `translate(0, ${20 * categoryIndex})`;
      }
    };
    var legend = (
      d3SVG
        .selectAll("g")
        .data(
          colors
            .domain()
        )
        .enter()
        .append("g")
          .attr(legendAttributes)
    );

    // Добавить квадраты к легенде.
    var legendRectanglesAttributes = {
      width: 18,
      height: 18
    };
    legend
      .append("rect")
      .attr(legendRectanglesAttributes)
      .style({
        fill: colors
      });

    // Добавить текст к легенде.
    var legendTextsAttrbutes = {
      x: 24,
      y: 9,
      dy: ".35em"
    };
    legend
      .append("text")
      .text(
        function (categoryName) {
         return categoryName;
        }
      )
      .attr(legendTextsAttrbutes);

    return d3SVG;
  }

  function drawDonutChart(data) {
    // Параметры диаграммы.
    const MARGIN = 20,
          WIDTH = 120,
          HEIGHT = 120,
          RADIUS = Math.min(WIDTH, HEIGHT) / 2;

    var svgElement = createSVG();
    var svgAttributes = {
      width: WIDTH + 2 * MARGIN,
      height: HEIGHT + 2 * MARGIN
    };
    var d3SVG = (
      d3.select(svgElement)
        .attr(svgAttributes)
    );

    var chartAttributes = {
      transform: `translate(${WIDTH / 2 + MARGIN}, ${HEIGHT / 2 + MARGIN})`
    };
    var donutChart = (
      d3SVG
        .append("g")
        .attr(chartAttributes)
    );

    var colors = getColors();

    var pie = (
      d3.layout
        .pie()
        .sort(null)
        .value(function(categoryDatum, datumIndex) {
          var value = parseFloat(categoryDatum["Число"]);
          return value;
        })
    );

    var arc = (
      d3.svg
        .arc()
        .innerRadius(RADIUS * 0.6)
        .outerRadius(RADIUS * 1.0)
    );

    var donutArcs = (
      donutChart
        .selectAll(".arc")
        .data(pie(data))
        .enter()
        .append("g")
          .attr({
            "class": "arc"
          })
    );

    donutArcs.append("path")
      .attr({
        d: arc
      })
      .style({
        fill: function (categoryDatum, datumIndex) {
          return colors(datumIndex);
        }
      });

    // var textAttributes = {
    //   transform: function (categoryDatum) {
    //     return `translate(${-RADIUS}, ${RADIUS * 1.1})`
    //   },
    //   dy: "0.35em",
    //   width: WIDTH
    // };
    // donutArcs
    //   .append("text")
    //   .attr(textAttributes)
    //   .text(function(categoryDatum) {
    //     var categoryName = categoryDatum.data["Название"];
    //     return categoryName;
    //   });

    return d3SVG;
  }

  function createSVG() {
    var svgElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");

    return svgElement;
  }

  function getColors() {
    return (
      d3.scale
        .category20c()
    );

    // return (
    //   d3.scale
    //     .ordinal()
    //     .range([
    //       "#EF3B39", "#FFCD05", "#69C9CA", "#666699", "#CC3366",
    //       "#0099CC", "#CCCB31", "#009966", "#C1272D", "#F79420",
    //       "#445CA9", "#999999", "#402312", "#272361", "#A67C52",
    //       "#016735", "#F1AAAF", "#FBF5A2", "#A0E6DA", "#C9A8E2",
    //       "#F190AC", "#7BD2EA", "#DBD6B6", "#6FE4D0"
    //     ])
    // );
  }
});