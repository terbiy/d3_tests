new d3Helpers(["svg", "data", "allCharts", "barChart"], function (helpers) {
  var data = [
    {abba1: 0.13},
    {abba2: 0.44},
    {abba3: 0.08},
    {abba4: 1.1},
    {abba13: 0.97}
  ];
  var style = {
    margin: 60,
    width: 400,
    height: 300,
    bandInterval: 0.4
  };

  var barChart = helpers.createBarChart(data, style),
      body = document.querySelector("body");

  body.appendChild(barChart.node());

  var newData = [
    { abba1: 0.49 },
    { tomato: 0.31 },
    { potato: 1.9 },
    { carrot: 0.4 }
  ];

  barChart.on("click", function () {
    helpers.updateBarChart(barChart, newData);

    for (
      let datumIndex = 0;
      datumIndex < newData.length;
      datumIndex++
    ) {
      let datum = newData[datumIndex];

      for (let property in datum) {
        if (datum.hasOwnProperty(property)) {
          datum[property] += Math.random();
        }
      }
    }
  })
});