// Шаблон «Глобальный конструктор» из книги «JavaScript Шаблоны».
function Common() {
  "use strict";
  // создаём массив аргументов.
  var args = Array.prototype.slice.call(arguments),
      callback = args.pop(),
      // Модули могут передаваться в форме массива или
      // в форме отдельных параметров.
      modules = (args[0] && typeof args[0] === "string") ? args : args[0],
      moduleName,
      moduleIndex;

  if (!(this instanceof Common)) {
    return new Common(modules, callback);
  }

  // добавить модули в базовый объект this
  // отсутствие аргументов с именами модулей или аргумент со значением "*"
  // предполагает необходимость включения всех модулей.
  if(!modules || modules === "*") {
    modules = [];
    for (moduleName in Common.modules) {
      if (Common.modules.hasOwnProperty(moduleName)) {
        modules.push(moduleName);
      }
    }
  }

  // инициализация выбранных модулей
  for (moduleIndex = 0; moduleIndex < modules.length; moduleIndex++) {
    Common.modules[modules[moduleIndex]](this);
  }

  callback(this);
}

Common.modules = {};

Common.modules.carry = function (common) {
  // Функция для осуществления каррирования или
  // шейнфинкелизации. В память о русском математике
  // Моисее Исаевиче Шейнфинкеле.
  // Взята полностью из книги Стояна Стефанова «JavaScript Шаблоны».
  common.carry = function (fn) {
    var slice = Array.prototype.slice,
        storedArguments = slice.call(arguments, 1);

    return function () {
      var newArguments = slice.call(arguments),
          args = storedArguments.concat(newArguments);

      return fn.apply(null, args);
    }
  }
}

Common.modules.analysis = function (common) {
  common.logOwnProperties = function (object) {
    for (var property in object) {
      console.log(`${property}: ${typeof object[property]}`);
      console.log(object[property]);
    }
  }
};


Common.modules.visualModifications = function (common) {
  common.getTranslationString = function (left, top) {
    return `translate(${left}, ${top})`;
  }

  common.color = (
    d3.scale
      .ordinal()
      .range([
        "#EF3B39", "#FFCD05", "#69C9CA", "#666699", "#CC3366", 
        "#0099CC", "#CCCB31", "#009966", "#C1272D", "#F79420", 
        "#445CA9", "#999999", "#402312", "#272361", "#A67C52", 
        "#016735", "#F1AAAF", "#FBF5A2", "#A0E6DA", "#C9A8E2", 
        "#F190AC", "#7BD2EA", "#DBD6B6", "#6FE4D0"
      ])
  );

  common.fixateColors = function (data) {
    this.color
      .domain(
        this.uniques(data, function (datum) {
          return datum.from;
        })
      );
  };

  common.arcLabels = function (text, radius) {
    var self = this;
    return function (selection) {
      selection
        .append("text")
        .text(text)
        .attr({
          "text-anchor": function (datum) {
            return self.tickAngle(datum) > 100 ? "end" : "start";
          }
        })
        .attr({
          transform: function (datum) {
            var degrees = self.tickAngle(datum);
            var turn = `rotate(${degrees}) translate(${radius(datum) + 10}, 0)`;

            if (degrees > 100) {
              turn += "rotate(180)";
            }

            return turn;
          }
        });
    }
  }

  common.tickAngle = function (datum) {
    var midAngle = (datum.endAngle - datum.startAngle) / 2,
        degrees = (midAngle + datum.startAngle) / Math.PI * 180 - 90;

    return degrees;
  };
}